import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.scss';
import Header from "./components/Header";
import Home from './pages/Home';
import ExpensifyDashPage from './pages/ExpensifyDashPage';
import AddExpensePage from './pages/AddExpense';
import EditPage from './pages/EditPage';
import HelpPage from './pages/HelpPage';
import NotFoundPage from './pages/NotFound';


class App extends Component {
  render() {
    return (
        <BrowserRouter>
              <div className="App">
                <Header />
                <div className="container">
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/dashboard" component={ExpensifyDashPage}/>
                        <Route path="/create" component={AddExpensePage}/>
                        <Route path="/edit/:id" component={EditPage}/>
                        <Route path="/help" component={HelpPage}/>
                        <Route path="*" component={NotFoundPage}/>
                    </Switch>
                </div>
              </div>
        </BrowserRouter>
    );
  }
}

export default App;
