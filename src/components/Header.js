import React, {Component} from 'react';
import { NavLink, Link } from 'react-router-dom';


class Header extends Component {
    render() {
        return (
            <div>
                <nav className="nav-bar">
                    <div className="nav-wrapper expensify-nav">
                        <a className="brand-logo">
                            <Link to="/">
                                App
                            </Link>
                        </a>
                        <ul id="nav-mobile" className="right hide-on-med-and-down">
                            <li>
                                <NavLink to="/dashboard" exact activeClassName="active">
                                    Dashboard
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/create" activeClassName="active">
                                    Add
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/help" activeClassName="active">
                                    Help
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </nav>

                {/*<AppBar className="nav-bar" color="white">*/}
                    {/*<Toolbar>*/}
                        {/*<Typography variant="title">*/}
                            {/*<Link to="/">Expensify</Link>*/}
                        {/*</Typography>*/}
                    {/*</Toolbar>*/}
                {/*</AppBar>*/}


            </div>
        );
    }
}

Header.propTypes = {};

export default Header;
